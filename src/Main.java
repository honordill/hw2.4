/*hw4)Найти медиану произвольного целочисленного ряда(массива, пример {4,2,6,-1,4,0,5,-34,8,23,10}).
Для сортировки, используйте сортировку
выбора(постарайтесь сами реализовать данную сортировку)*/
public class Main {
    public static void main(String[] args) {
        int[] mas = {4, 2, 6, -1, 4, 0, 5, -34, 8, 23, 10};
        for (int i = 0; i < mas.length - 1; i++)
            for (int j = 0; j < mas.length - 1; j++)
                if (mas[j] > mas[j + 1]) {
                    int temp = mas[j];
                    mas[j] = mas[j + 1];
                    mas[j + 1] = temp;
                }
         System.out.println(mas[mas.length/2]);
    }
}
